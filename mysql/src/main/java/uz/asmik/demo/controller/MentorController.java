package uz.asmik.demo.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.asmik.demo.payload.ApiResponse;
import uz.asmik.demo.payload.MentorRequest;
import uz.asmik.demo.service.MentorService;
import uz.asmik.demo.utills.AppCons;

@Controller
@RequestMapping("/mentor")
public class MentorController {

    final MentorService mentorService;

    public MentorController(MentorService mentorService) {
        this.mentorService = mentorService;
    }

    @PostMapping
    public HttpEntity<?> mentorSaveOrEdit(@RequestBody MentorRequest mentorRequest){
        ApiResponse apiResponse=mentorService.mentorSaveOrEdit(mentorRequest);
        return ResponseEntity.status(apiResponse.isSuccess()?apiResponse.getMessage().equals("Saved")?
                HttpStatus.CREATED:HttpStatus.ACCEPTED:HttpStatus.CONFLICT).body(apiResponse);//201,202,409

    }

    @GetMapping("/all")
    public HttpEntity<?> AllMentors(){
        ApiResponse apiResponse=mentorService.getAllMentors();
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }


    @GetMapping("/allByPage")
    public HttpEntity<?> getAllByPage(@RequestParam(value = "page",defaultValue = AppCons.DEFAULT_PAGE_NUMBER) Integer page,
                                      @RequestParam(value = "size",defaultValue = AppCons.DEFAULT_PAGE_SIZE) Integer size){
        ApiResponse apiResponse=mentorService.getByPageable(page,size);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getOneMentor(@PathVariable Integer id){
        ApiResponse apiResponse=mentorService.getById(id);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }

    @DeleteMapping("/delete/{id}")
    public HttpEntity<?> deleteGroup(@PathVariable Integer id){
        ApiResponse apiResponse=mentorService.deleteMentor(id);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }

}
