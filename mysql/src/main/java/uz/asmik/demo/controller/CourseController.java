package uz.asmik.demo.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.asmik.demo.payload.ApiResponse;
import uz.asmik.demo.payload.CourseRequest;
import uz.asmik.demo.service.CourseService;
import uz.asmik.demo.utills.AppCons;

@Controller
@RequestMapping("/course")
public class CourseController {

    final CourseService courseService;

    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @PostMapping
    public HttpEntity<?> studentSaveOrEdit(@RequestBody CourseRequest courseRequest){
        ApiResponse apiResponse=courseService.courseSaveOrEdit(courseRequest);
        return ResponseEntity.status(apiResponse.isSuccess()?apiResponse.getMessage().equals("Saved")?
                HttpStatus.CREATED:HttpStatus.ACCEPTED:HttpStatus.CONFLICT).body(apiResponse);//201,202,409

    }

    @GetMapping("/all")
    public HttpEntity<?> AllCourses(){
        ApiResponse apiResponse=courseService.getAllCourses();
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }


    @GetMapping("/allByPage")
    public HttpEntity<?> getAllByPage(@RequestParam(value = "page",defaultValue = AppCons.DEFAULT_PAGE_NUMBER) Integer page,
                                      @RequestParam(value = "size",defaultValue = AppCons.DEFAULT_PAGE_SIZE) Integer size){
        ApiResponse apiResponse=courseService.getByPageAble(page,size);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getOneCourse(@PathVariable Integer id){
        ApiResponse apiResponse=courseService.getOne(id);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }

    @DeleteMapping("/delete/{id}")
    public HttpEntity<?> deleteCourse(@PathVariable Integer id){
        ApiResponse apiResponse=courseService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }


}
