package uz.asmik.demo.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.asmik.demo.payload.ApiResponse;
import uz.asmik.demo.payload.LearningCenterRequest;
import uz.asmik.demo.service.LearningCenterService;
import uz.asmik.demo.utills.AppCons;

@Controller
@RequestMapping("/center")
public class LearningCenterController {

    final LearningCenterService learningCenterService;

    public LearningCenterController(LearningCenterService learningCenterService) {
        this.learningCenterService = learningCenterService;
    }

    @PostMapping
    public HttpEntity<?> studentSaveOrEdit(@RequestBody LearningCenterRequest learningCenterRequest){
        ApiResponse apiResponse=learningCenterService.courseSaveOrEdit(learningCenterRequest);
        return ResponseEntity.status(apiResponse.isSuccess()?apiResponse.getMessage().equals("Saved")?
                HttpStatus.CREATED:HttpStatus.ACCEPTED:HttpStatus.CONFLICT).body(apiResponse);//201,202,409

    }

    @GetMapping("/all")
    public HttpEntity<?> AllCourses(){
        ApiResponse apiResponse=learningCenterService.getAllCenters();
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }


    @GetMapping("/allByPage")
    public HttpEntity<?> getAllByPage(@RequestParam(value = "page",defaultValue = AppCons.DEFAULT_PAGE_NUMBER) Integer page,
                                      @RequestParam(value = "size",defaultValue = AppCons.DEFAULT_PAGE_SIZE) Integer size){
        ApiResponse apiResponse=learningCenterService.getByPageAble(page,size);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getOneCourse(@PathVariable Integer id){
        ApiResponse apiResponse=learningCenterService.getOne(id);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }

    @DeleteMapping("/delete/{id}")
    public HttpEntity<?> deleteCourse(@PathVariable Integer id){
        ApiResponse apiResponse=learningCenterService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }

    @GetMapping("/groupInfo")
    public HttpEntity<?> getGroupInfo(@RequestParam Integer id){
        ApiResponse apiResponse=learningCenterService.getGroupInfo(id);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }



}
