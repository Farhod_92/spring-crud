package uz.asmik.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.asmik.demo.payload.ApiResponse;
import uz.asmik.demo.payload.StudentRequest;
import uz.asmik.demo.service.StudentServ;
import uz.asmik.demo.utills.AppCons;

@Controller
@RequestMapping("/student")
public class StudentController {

    @Autowired
    StudentServ studentService;

    @PostMapping
    public HttpEntity<?> studentSaqlashYokiTahrirlash(@RequestBody StudentRequest reqStudent){
        ApiResponse apiResponse = studentService.studentSaveOrEdit(reqStudent);
        return ResponseEntity.status(apiResponse.isSuccess()?apiResponse.getMessage().equals("Saved")?201:202:409).body(apiResponse);
    }

    @GetMapping("/all")
    public HttpEntity<?> getAllStudents(){
        ApiResponse apiResponse = studentService.getAllStudents();
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);

    }

    @GetMapping("/getByPageable")
    public HttpEntity<?> getByPageable(@RequestParam(value = "page",defaultValue = AppCons.DEFAULT_PAGE_NUMBER)Integer page,
                                       @RequestParam(value = "size",defaultValue = AppCons.DEFAULT_PAGE_SIZE)Integer size){
        ApiResponse apiResponse = studentService.getByPageable(page,size);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);

    }

    @GetMapping("/{id}")
    public HttpEntity<?> getAllStudents(@PathVariable Integer id){
        ApiResponse apiResponse = studentService.getById(id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);

    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteStudent(@PathVariable Integer id){
        ApiResponse apiResponse = studentService.deleteStudent(id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);

    }

    @GetMapping("/search")
    public HttpEntity<?> searchStudent(@RequestParam String s){
        ApiResponse apiResponse = studentService.searchStudent(s);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }



}
