package uz.asmik.demo.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.asmik.demo.payload.ApiResponse;
import uz.asmik.demo.payload.TypeRequest;
import uz.asmik.demo.service.TypeService;
import uz.asmik.demo.utills.AppCons;

@Controller
@RequestMapping("/type")
public class TypeController {
    final TypeService typeService;

    public TypeController(TypeService typeService) {
        this.typeService = typeService;
    }

    @PostMapping
    public HttpEntity<?> studentSaveOrEdit(@RequestBody TypeRequest typeRequest){
        ApiResponse apiResponse=typeService.typeSaveOrEdit(typeRequest);
        return ResponseEntity.status(apiResponse.isSuccess()?apiResponse.getMessage().equals("Saved")?
                HttpStatus.CREATED:HttpStatus.ACCEPTED:HttpStatus.CONFLICT).body(apiResponse);//201,202,409

    }

    @GetMapping("/all")
    public HttpEntity<?> AllTypes(){
        ApiResponse apiResponse=typeService.getAllTypes();
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }


    @GetMapping("/allByPage")
    public HttpEntity<?> getAllByPage(@RequestParam(value = "page",defaultValue = AppCons.DEFAULT_PAGE_NUMBER) Integer page,
                                      @RequestParam(value = "size",defaultValue = AppCons.DEFAULT_PAGE_SIZE) Integer size){
        ApiResponse apiResponse=typeService.getByPageAble(page,size);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getOneType(@PathVariable Integer id){
        ApiResponse apiResponse=typeService.getOne(id);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }

    @DeleteMapping("/delete/{id}")
    public HttpEntity<?> deleteCourse(@PathVariable Integer id){
        ApiResponse apiResponse=typeService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }

}
