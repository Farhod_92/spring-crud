package uz.asmik.demo.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.asmik.demo.payload.ApiResponse;
import uz.asmik.demo.payload.GroupRequest;
import uz.asmik.demo.service.GroupService;
import uz.asmik.demo.utills.AppCons;

@Controller
@RequestMapping("/group")
public class GroupController {

    final GroupService groupService;

    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @PostMapping
    public HttpEntity<?> studentSaveOrEdit(@RequestBody GroupRequest groupRequest){
        ApiResponse apiResponse=groupService.courseSaveOrEdit(groupRequest);
        return ResponseEntity.status(apiResponse.isSuccess()?apiResponse.getMessage().equals("Saved")?
                HttpStatus.CREATED:HttpStatus.ACCEPTED:HttpStatus.CONFLICT).body(apiResponse);//201,202,409

    }

    @GetMapping("/all")
    public HttpEntity<?> AllGroups(){
        ApiResponse apiResponse=groupService.getAllCourses();
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }


    @GetMapping("/allByPage")
    public HttpEntity<?> getAllByPage(@RequestParam(value = "page",defaultValue = AppCons.DEFAULT_PAGE_NUMBER) Integer page,
                                      @RequestParam(value = "size",defaultValue = AppCons.DEFAULT_PAGE_SIZE) Integer size){
        ApiResponse apiResponse=groupService.getByPageAble(page,size);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getOneGroup(@PathVariable Integer id){
        ApiResponse apiResponse=groupService.getOne(id);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }

    @DeleteMapping("/delete/{id}")
    public HttpEntity<?> deleteGroup(@PathVariable Integer id){
        ApiResponse apiResponse=groupService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK:HttpStatus.CONFLICT).body(apiResponse);//200,409
    }


}
