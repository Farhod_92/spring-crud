package uz.asmik.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.asmik.demo.entity.Course;

import java.util.List;

public interface CourseRepository extends JpaRepository<Course,Integer> {
    List<Course> findAllByLearningCenterId(Integer learningCenter_id);
}
