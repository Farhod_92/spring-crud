package uz.asmik.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.asmik.demo.entity.Region;


@RepositoryRestResource(path = "region",collectionResourceRel = "list")
public interface RegionRepo extends JpaRepository<Region,Integer> {
}
