package uz.asmik.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.PathVariable;
import uz.asmik.demo.entity.Mentor;
import uz.asmik.demo.entity.Student;

import java.util.List;

public interface MentorRepository extends JpaRepository<Mentor,Integer> {
    @Query(value = "select * from mentor where id in(select mentor_id from mentor_groups where groups_id=:groupId);",nativeQuery = true)
    List<Mentor> allByGroup(@PathVariable(value = "groupId")Integer groupId);
}
