package uz.asmik.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.asmik.demo.entity.Groups;

import java.util.List;

public interface GroupRepository  extends JpaRepository<Groups,Integer> {
    List<Groups> findAllByCourseLearningCenterId(Integer course_learningCenter_id);
}
