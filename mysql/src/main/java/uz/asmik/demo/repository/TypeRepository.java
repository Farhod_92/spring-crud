package uz.asmik.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.asmik.demo.entity.Type;

public interface TypeRepository extends JpaRepository<Type,Integer> {
}
