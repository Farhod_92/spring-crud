package uz.asmik.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.PathVariable;
import uz.asmik.demo.entity.Student;

import java.util.List;

public interface StudentRepo extends JpaRepository<Student,Integer> {
    List<Student> findAllByNameStartingWithIgnoreCase(String qidiruvSozi);

    @Query(value = "select * from student where id in(select student_id from student_groups where groups_id=:groupId);",nativeQuery = true)
    List<Student> allByGroup(@PathVariable(value = "groupId")Integer groupId);

}
