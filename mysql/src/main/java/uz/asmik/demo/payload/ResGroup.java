package uz.asmik.demo.payload;

import lombok.Data;
import uz.asmik.demo.entity.Mentor;
import uz.asmik.demo.entity.Student;

import java.util.List;

@Data
public class ResGroup {
    private Integer id;
    private String name;
    private ResCouurce resCouurce;
    private List<Student> studentList;
    private List<Mentor> mentorList;

}
