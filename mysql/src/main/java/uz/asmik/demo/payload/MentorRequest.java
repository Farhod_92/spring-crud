package uz.asmik.demo.payload;

import lombok.Data;

import java.util.List;

@Data
public class MentorRequest {
    private Integer id;
    private String name;
    private List<Integer> groupsIds;
}
