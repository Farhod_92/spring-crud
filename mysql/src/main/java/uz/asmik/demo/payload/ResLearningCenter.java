package uz.asmik.demo.payload;

import lombok.Data;

import java.util.List;

@Data
public class ResLearningCenter {
    private Integer id;
    private String name;
    private List<ResCouurce> resCouurceList;
    private List<ResGroup> resGroupList;
}
