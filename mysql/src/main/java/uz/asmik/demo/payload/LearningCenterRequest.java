package uz.asmik.demo.payload;

import lombok.Data;

@Data
public class LearningCenterRequest {
    private  Integer id;
    private String name;
}
