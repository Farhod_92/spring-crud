package uz.asmik.demo.payload;

import lombok.Data;

import java.util.List;

@Data
public class StudentRequest {
    private Integer id;
    private String name;
    private List<Integer> groupsIds;
}
