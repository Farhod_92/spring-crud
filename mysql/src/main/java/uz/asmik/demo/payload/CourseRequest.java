package uz.asmik.demo.payload;

import lombok.Data;

@Data
public class CourseRequest {
    private Integer id;
    private String name;
    private Integer learning_center;
    private Integer type;
}
