package uz.asmik.demo.payload;

import uz.asmik.demo.entity.Course;
import uz.asmik.demo.entity.Groups;
import uz.asmik.demo.entity.Mentor;
import uz.asmik.demo.entity.Student;

import java.util.List;


public class ResLcInfo {
    private  Integer id;
    private  String name;
    private List<Course> courseList;
    private List<Groups> groupsList;
    private List<Student> studentList;
    private List<Mentor> mentorList;
}
