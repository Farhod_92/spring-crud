package uz.asmik.demo.payload;

import lombok.Data;

@Data
public class GroupRequest {
    private Integer id;
    private String name;
    private Integer course;
}
