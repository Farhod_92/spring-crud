package uz.asmik.demo.payload;

import lombok.Data;

@Data
public class TypeRequest {
    private Integer id;
    private String name;
}
