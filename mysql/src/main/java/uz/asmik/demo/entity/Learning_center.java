package uz.asmik.demo.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.asmik.demo.entity.template.AbsEntity;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Learning_center extends AbsEntity {

}
