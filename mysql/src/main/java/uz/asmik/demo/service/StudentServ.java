package uz.asmik.demo.service;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import uz.asmik.demo.entity.Groups;
import uz.asmik.demo.entity.Student;
import uz.asmik.demo.payload.ApiResponse;
import uz.asmik.demo.payload.StudentRequest;
import uz.asmik.demo.repository.GroupRepository;
import uz.asmik.demo.repository.StudentRepo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class StudentServ {
    final    StudentRepo studentRepo;
    final GroupRepository groupRepository;

    public StudentServ(StudentRepo studentRepo, GroupRepository groupRepository) {
        this.studentRepo = studentRepo;
        this.groupRepository = groupRepository;
    }

    public ApiResponse studentSaveOrEdit(StudentRequest reqStudent){
       try {
           Student student=new Student();
           if (reqStudent.getId()!=null){
                student= studentRepo.findById(reqStudent.getId()).orElse(null);
           }
           student.setName(reqStudent.getName());
           List<Groups> groupsList=new ArrayList<>();

           for (Integer groupsId : reqStudent.getGroupsIds()) {
               Optional<Groups> groups=groupRepository.findById(groupsId);
               if(groups.isPresent()){
                   if(student.getGroups()==null){
                       groupsList.add(groups.get());
                   }else{
                       groupsList.addAll(student.getGroups());
                      groupsList.add(groups.get());
                   }
               }
           }
           student.setGroups(groupsList);

          // groupRepository.save(groups.get());

           Student savedStudent = studentRepo.save(student);
           return new ApiResponse(true,reqStudent.getId()!=null?"Edited":"Saved",savedStudent);
       }catch (Exception e){
           return new ApiResponse(false,"Error",e);
       }
   }

    public ApiResponse getAllStudents() {
        return new ApiResponse(true,"All Students",studentRepo.findAll());
    }

    public ApiResponse getByPageable(Integer page, Integer size) {
        return new ApiResponse(true,"All Students",studentRepo.findAll(PageRequest.of(page, size)));
    }


    public ApiResponse getById(Integer id) {
        Optional<Student> optionalStudent = studentRepo.findById(id);
        if (optionalStudent.isPresent()){
            Student student = optionalStudent.get();
            return new ApiResponse(true,"Ok",student);
        }
        return new ApiResponse(false,"Error",null);
    }

    public ApiResponse deleteStudent(Integer id) {
       try {
               studentRepo.deleteById(id);
               return new ApiResponse(true,"Ok",null);
       }catch (Exception e){
           return new ApiResponse(false,"Error",e);
       }
    }

    public ApiResponse searchStudent(String s) {
       return new ApiResponse(true,"st",studentRepo.findAllByNameStartingWithIgnoreCase(s));
    }


}
