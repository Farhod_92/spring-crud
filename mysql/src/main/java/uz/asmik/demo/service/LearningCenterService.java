package uz.asmik.demo.service;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import uz.asmik.demo.entity.Course;
import uz.asmik.demo.entity.Groups;
import uz.asmik.demo.entity.Learning_center;
import uz.asmik.demo.payload.*;
import uz.asmik.demo.repository.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class LearningCenterService {
   final LarningCenterrepository larningCenterrepository;
   final CourseRepository courseRepository;
   final GroupRepository groupRepository;
   final StudentRepo studentRepo;
   final MentorRepository mentorRepository;

    public LearningCenterService(LarningCenterrepository larningCenterrepository, CourseRepository courseRepository, GroupRepository groupRepository, StudentRepo studentRepo, MentorRepository mentorRepository) {
        this.larningCenterrepository = larningCenterrepository;
        this.courseRepository = courseRepository;
        this.groupRepository = groupRepository;
        this.studentRepo = studentRepo;
        this.mentorRepository = mentorRepository;
    }

    //Learning center save or edit
    public ApiResponse courseSaveOrEdit(LearningCenterRequest learningCenterRequest){
        try{
            Learning_center learning_center=new Learning_center();

            if(learningCenterRequest.getId()!=null){
                learning_center= larningCenterrepository.findById(learningCenterRequest.getId()).orElse(null);
            }

            learning_center.setName(learningCenterRequest.getName());

            Learning_center savedCenter=larningCenterrepository.save(learning_center);
            return new ApiResponse(true,learningCenterRequest.getId()==null?"Saved":"Edited",savedCenter);

        }catch (Exception ex){
            return new ApiResponse(false,"Error",ex);
        }

    }

    //get all Learning centers
    public ApiResponse getAllCenters(){
        return new ApiResponse(true,"All courses",larningCenterrepository.findAll());
    }

    //get all Centers by page
    public ApiResponse getByPageAble(Integer page,Integer size){
        return new ApiResponse(true,"Courses by page",larningCenterrepository.findAll(PageRequest.of(page,size)));
    }

    //get one Center by id
    public ApiResponse getOne(Integer id){
        Optional<Learning_center> optionalCenter=larningCenterrepository.findById(id);
        if(optionalCenter.isPresent()){
            Learning_center center=optionalCenter.get();
            return new ApiResponse(true,"your course",center);
        }
        return new
                ApiResponse(false,"No group by this id",null);
    }

    //delete Center
    public ApiResponse delete(Integer id){
        try{
            larningCenterrepository.deleteById(id);
            return new ApiResponse(true,"deleted",null);
        }catch (Exception ex){
            return new ApiResponse(false,"Error",ex);
        }
    }

    public ApiResponse getGroupInfo(Integer id) {
        try {
            ResLearningCenter resLearningCenter=new ResLearningCenter();
            Learning_center learning_center = larningCenterrepository.findById(id).orElse(null);
            resLearningCenter.setId(learning_center.getId());
            resLearningCenter.setName(learning_center.getName());
            resLearningCenter.setResCouurceList(courseRepository.findAllByLearningCenterId(id).stream().map(this::gerResCource).collect(Collectors.toList()));
            resLearningCenter.setResGroupList(groupRepository.findAllByCourseLearningCenterId(id).stream().map(this::getResGroup).collect(Collectors.toList()));
            return new ApiResponse(true,"Ok",resLearningCenter);
        }catch (Exception e){
            return new ApiResponse(false,"Error",null);
        }

    }

    public ResCouurce gerResCource(Course course){
        ResCouurce resCouurce=new ResCouurce();
        resCouurce.setId(course.getId());
        resCouurce.setName(course.getName());
        resCouurce.setCourceTypeName(course.getType().getName());
        return resCouurce;
    }

    public ResGroup getResGroup(Groups groups){
        ResGroup resGroup=new ResGroup();
        resGroup.setId(groups.getId());
        resGroup.setName(groups.getName());
        resGroup.setResCouurce(gerResCource(groups.getCourse()));
        resGroup.setMentorList(mentorRepository.allByGroup(groups.getId()));
        resGroup.setStudentList(studentRepo.allByGroup(groups.getId()));
        return resGroup;
    }
}
