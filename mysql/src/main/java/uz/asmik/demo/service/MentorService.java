package uz.asmik.demo.service;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import uz.asmik.demo.entity.Groups;
import uz.asmik.demo.entity.Mentor;
import uz.asmik.demo.payload.ApiResponse;
import uz.asmik.demo.payload.MentorRequest;
import uz.asmik.demo.repository.GroupRepository;
import uz.asmik.demo.repository.MentorRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class MentorService {
    final MentorRepository mentorRepository;
    final GroupRepository groupRepository;

    public MentorService(MentorRepository mentorRepository, GroupRepository groupRepository) {
        this.mentorRepository = mentorRepository;
        this.groupRepository = groupRepository;
    }

    public ApiResponse mentorSaveOrEdit(MentorRequest mentorRequest){
        try {
            Mentor mentor=new Mentor();
            if (mentorRequest.getId()!=null){
                mentor= mentorRepository.findById(mentorRequest.getId()).orElse(null);
            }
            mentor.setName(mentorRequest.getName());
            List<Groups> groupsList=new ArrayList<>();

            for (Integer groupsId : mentorRequest.getGroupsIds()) {
                Optional<Groups> groups=groupRepository.findById(groupsId);
                if(groups.isPresent()){
                    if(mentor.getGroups()==null){
                        groupsList.add(groups.get());
                    }else{
                        groupsList.addAll(mentor.getGroups());
                        groupsList.add(groups.get());
                    }
                }
            }
            mentor.setGroups(groupsList);

           // groups.get().getMentors().add(mentor);

            Mentor savedMentor= mentorRepository.save(mentor);
            return new ApiResponse(true,mentorRequest.getId()!=null?"Edited":"Saved",savedMentor);
        }catch (Exception e){
            return new ApiResponse(false,"Error",e);
        }
    }

    public ApiResponse getAllMentors() {
        return new ApiResponse(true,"All Mentors",mentorRepository.findAll());
    }

    public ApiResponse getByPageable(Integer page, Integer size) {
        return new ApiResponse(true,"All Mentors",mentorRepository.findAll(PageRequest.of(page, size)));
    }


    public ApiResponse getById(Integer id) {
        Optional<Mentor> optionalMentor= mentorRepository.findById(id);
        if (optionalMentor.isPresent()){
            Mentor mentor = optionalMentor.get();
            return new ApiResponse(true,"Ok",mentor);
        }
        return new ApiResponse(false,"Error",null);
    }

    public ApiResponse deleteMentor(Integer id) {
        try {
            mentorRepository.deleteById(id);
            return new ApiResponse(true,"Ok",null);
        }catch (Exception e){
            return new ApiResponse(false,"Error",e);
        }
    }
}
