package uz.asmik.demo.service;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import uz.asmik.demo.entity.Course;
import uz.asmik.demo.entity.Learning_center;
import uz.asmik.demo.entity.Type;
import uz.asmik.demo.payload.ApiResponse;
import uz.asmik.demo.payload.CourseRequest;
import uz.asmik.demo.repository.CourseRepository;
import uz.asmik.demo.repository.LarningCenterrepository;
import uz.asmik.demo.repository.TypeRepository;

import java.util.Optional;

@Service
public class CourseService {
    final CourseRepository courseRepository;
    final LarningCenterrepository learningCenterrepository;
    final TypeRepository typeRepository;

    public CourseService(CourseRepository courseRepository, LarningCenterrepository learningCenterrepository, TypeRepository typeRepository) {
        this.courseRepository = courseRepository;
        this.learningCenterrepository = learningCenterrepository;
        this.typeRepository = typeRepository;
    }

    //course save or edit
    public ApiResponse courseSaveOrEdit(CourseRequest courseRequest){
        try{
            Course course=new Course();

            if(courseRequest.getId()!=null){
                course= courseRepository.findById(courseRequest.getId()).orElse(null);
            }

            course.setName(courseRequest.getName());

            Optional<Learning_center> learning_center=learningCenterrepository.findById(courseRequest.getLearning_center());
            if(!learning_center.isPresent())
                return new ApiResponse(false,"Error",null);
            course.setLearningCenter(learning_center.get());
            Optional<Type> type=typeRepository.findById(courseRequest.getType());
            if(!type.isPresent())
                return new ApiResponse(false,"Error",null);
            course.setType(type.get());
            Course savedCourse=courseRepository.save(course);
            return new ApiResponse(true,courseRequest.getId()==null?"Saved":"Edited",savedCourse);

        }catch (Exception ex){
            return new ApiResponse(false,"Error",ex);
        }

    }

    //get all courses
    public ApiResponse getAllCourses(){
        return new ApiResponse(true,"All courses",courseRepository.findAll());
    }
    //get all courses by page
    public ApiResponse getByPageAble(Integer page,Integer size){
        return new ApiResponse(true,"Courses by page",courseRepository.findAll(PageRequest.of(page,size)));
    }

    //get one course by id
    public ApiResponse getOne(Integer id){
        Optional<Course> optionalCourse=courseRepository.findById(id);
        if(optionalCourse.isPresent()){
            Course course=optionalCourse.get();
            return new ApiResponse(true,"your course",course);
        }
        return new
                ApiResponse(false,"No course by this id",null);
    }

    //delete course
    public ApiResponse delete(Integer id){
        try{
            courseRepository.deleteById(id);
            return new ApiResponse(true,"deleted",null);
        }catch (Exception ex){
            return new ApiResponse(false,"Error",ex);
        }
    }

}
