package uz.asmik.demo.service;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import uz.asmik.demo.entity.Course;
import uz.asmik.demo.entity.Groups;
import uz.asmik.demo.payload.ApiResponse;
import uz.asmik.demo.payload.GroupRequest;
import uz.asmik.demo.repository.CourseRepository;
import uz.asmik.demo.repository.GroupRepository;

import java.util.Optional;

@Service
public class GroupService {
    final CourseRepository courseRepository;
    final GroupRepository groupRepository;


    public GroupService(CourseRepository courseRepository, GroupRepository groupRepository) {
        this.courseRepository = courseRepository;
        this.groupRepository = groupRepository;
    }

    //group save or edit
    public ApiResponse courseSaveOrEdit(GroupRequest groupRequest){
        try{
            Groups groups=new Groups();

            if(groupRequest.getId()!=null){
                groups= groupRepository.findById(groupRequest.getId()).orElse(null);
            }

            groups.setName(groupRequest.getName());

            Optional<Course> course=courseRepository.findById(groupRequest.getCourse() );
            groups.setCourse(course.get());

            Groups savedGroup=groupRepository.save(groups);
            return new ApiResponse(true,groupRequest.getId()==null?"Saved":"Edited",savedGroup);

        }catch (Exception ex){
            return new ApiResponse(false,"Error",ex);
        }

    }

    //get all courses
    public ApiResponse getAllCourses(){
        return new ApiResponse(true,"All courses",groupRepository.findAll());
    }
    //get all courses by page
    public ApiResponse getByPageAble(Integer page,Integer size){
        return new ApiResponse(true,"Courses by page",groupRepository.findAll(PageRequest.of(page,size)));
    }

    //get one course by id
    public ApiResponse getOne(Integer id){
        Optional<Groups> optionalGroups=groupRepository.findById(id);
        if(optionalGroups.isPresent()){
            Groups groups=optionalGroups.get();
            return new ApiResponse(true,"your course",groups);
        }
        return new
                ApiResponse(false,"No group by this id",null);
    }

    //delete course
    public ApiResponse delete(Integer id){
        try{
            Optional<Groups> deletedGroup = groupRepository.findById(id);
            groupRepository.deleteById(id);
            return new ApiResponse(true,"deleted",deletedGroup);
        }catch (Exception ex){
            return new ApiResponse(false,"Error",ex);
        }
    }

}
