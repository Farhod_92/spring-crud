package uz.asmik.demo.service;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import uz.asmik.demo.entity.Type;
import uz.asmik.demo.payload.ApiResponse;
import uz.asmik.demo.payload.TypeRequest;
import uz.asmik.demo.repository.TypeRepository;

import java.util.Optional;

@Service
public class TypeService {
    final TypeRepository typeRepository;

    public TypeService(TypeRepository typeRepository) {
        this.typeRepository = typeRepository;
    }

    //Learning center save or edit
    public ApiResponse typeSaveOrEdit(TypeRequest typeRequest){
        try{
            Type type=new Type();

            if(typeRequest.getId()!=null){
                type= typeRepository.findById(typeRequest.getId()).orElse(null);
            }

            type.setName(typeRequest.getName());

            Type savedType=typeRepository.save(type);
            return new ApiResponse(true,typeRequest.getId()==null?"Saved":"Edited",savedType);

        }catch (Exception ex){
            return new ApiResponse(false,"Error",ex);
        }

    }

    //get all Learning centers
    public ApiResponse getAllTypes(){
        return new ApiResponse(true,"All courses",typeRepository.findAll());
    }

    //get all Centers by page
    public ApiResponse getByPageAble(Integer page,Integer size){
        return new ApiResponse(true,"Courses by page",typeRepository.findAll(PageRequest.of(page,size)));
    }

    //get one Center by id
    public ApiResponse getOne(Integer id){
        Optional<Type> optionalCenter=typeRepository.findById(id);
        if(optionalCenter.isPresent()){
            Type type=optionalCenter.get();
            return new ApiResponse(true,"your course",type);
        }
        return new
                ApiResponse(false,"No group by this id",null);
    }

    //delete Center
    public ApiResponse delete(Integer id){
        try{
            typeRepository.deleteById(id);
            return new ApiResponse(true,"deleted",null);
        }catch (Exception ex){
            return new ApiResponse(false,"Error",ex);
        }
    }

}
